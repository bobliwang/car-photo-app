import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CarsListComponent } from './cars-list/cars-list.component';
import { CarDetailsComponent } from './car-details/car-details.component';
import { appRoutes } from './app.routes';
import { SideNavBarComponent } from './side-nav-bar/side-nav-bar.component';
import { CarService } from './services/car.service';
import { AzurePhotoUploadService } from './services/azure-photo-upload.service';
import { AppStateService } from './services/app-state.service';



@NgModule({
  declarations: [
    AppComponent,
    CarsListComponent,
    CarDetailsComponent,
    SideNavBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [
    CarService,
    AzurePhotoUploadService,
    AppStateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private appState: AppStateService) {
    this.appState.initialize().subscribe(x => {
      console.log('appState - initialize - success');
    }, err => {
      console.error('appState - initialize - error');
    });
  }
}
