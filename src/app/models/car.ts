import { CarPhoto } from './car-photo';


export class Car {
    
    public id: number;

    public make: string;

    public model: string;

    public photos: CarPhoto[];
}
