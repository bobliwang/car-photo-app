import { AppStateService } from '../services/app-state.service';
import { Component, OnInit } from '@angular/core';
import { CarService } from '../services/car.service';
import { Car } from '../models';

@Component({
  selector: 'cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.css']
})
export class CarsListComponent implements OnInit {

  public cars: Car[];

  constructor(private carService: CarService, public appState: AppStateService ) {
    
  }

  ngOnInit() {
    
  }

}
