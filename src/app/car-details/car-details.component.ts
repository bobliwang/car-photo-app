import { AppStateService } from '../services/app-state.service';
import { AzurePhotoUploadService } from '../services/azure-photo-upload.service';
import { Observable } from 'rxjs/Rx';
import { ViewChild } from '@angular/core';
import { CarService } from '../services/car.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Car } from '../models';
import 'rxjs/add/operator/switchMap';
import { CarPhoto } from '../models/car-photo';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.css']
})
export class CarDetailsComponent implements OnInit {

  public car: Car;

  @ViewChild('fileUpload')
  public fileUploadElementRef: ElementRef;

  constructor(private router: Router, private activeRoute: ActivatedRoute,
    private carService: CarService,
    private appState: AppStateService,
    private uploadService: AzurePhotoUploadService) {    
    
  }

  ngOnInit() {

    this.activeRoute
        .params
        .subscribe(params => {
          const carId = parseInt(params["carId"]);

          if (carId) {
            this.appState.initialize().subscribe(() => {
              this.car = this.appState.getCarById(carId)
            });
          }
          
        });
  }

  public deletePhoto(photo: CarPhoto) {
    const index = this.car.photos.indexOf(photo);

    if (index > -1) {
      this.car.photos.splice(index, 1)
    }

    this.carService.updateCars(this.appState.cars);
  }

  public popupFileDialog() {
    if (this.fileUploadElementRef) {
      (<HTMLInputElement> this.fileUploadElementRef.nativeElement).click();
    }
  }

  public fileChange(event: Event) {
    const files = <FileList> (<HTMLInputElement> this.fileUploadElementRef.nativeElement).files;

    console.log('selected files:', files);

    this.uploadService.uploadPhoto(files[0])
        .subscribe(x => {
          console.log('uploadPhoto.result:', x);

          const photo = new CarPhoto();
          photo.url = x;

          this.car.photos.unshift(photo);

          this.carService.updateCars(this.appState.cars).subscribe(x => {
            console.log(x);
          });
        });
        
    
  }

}
