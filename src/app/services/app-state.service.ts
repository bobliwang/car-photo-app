import { Observable, Subject, Subscription } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Car } from '../models';
import { CarService } from './car.service';

@Injectable()
export class AppStateService {

  constructor(private carService: CarService) {
    
  }

  public cars: Car[];

  public init$: Observable<boolean>;

  public initialize(): Observable<boolean> {

    if (!this.init$) {
      const subj = new Subject<boolean>();
      this.carService.getCars().subscribe(x => {
        this.cars = x;
        subj.next(true);
      }, err => {
        subj.error(err);
      });
  
      this.init$ = subj.asObservable();  
    }
    
    return this.init$
  }

  public getCarById(id: number): Car {

    if (this.cars) {
      return this.cars.find(x => x.id === id);
    }

    return null;
  }
}
