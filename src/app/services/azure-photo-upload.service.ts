import { HttpClient, HttpEventType, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AzurePhotoUploadService {

  constructor(private httpClient: HttpClient) { }

  public uploadPhoto(file: File): Observable<string> {

    const subject = new Subject<string>();

    this.getUploadedUrl(file.name)
        .subscribe(url => {
          const req = new HttpRequest('PUT', url, file, {
            reportProgress: true,
            headers: new HttpHeaders({
              'x-ms-blob-type': 'BlockBlob'
            }),
            responseType: 'text'
          });

          this.httpClient.request(req).subscribe(evt => {

            console.log('HttpEvent:', evt);

            if (evt.type === HttpEventType.UploadProgress) {
              console.log('Progress');
            } else if (evt.type === HttpEventType.Response) {
              subject.next(evt.url);
            }
          });

        }, err => {
          subject.error(err);
        });

    return subject.asObservable();
  }

  /**
   * TODO: This method should talk to a web api endpoint to get the signed url for file upload.
   * @param fileName
   */
  private getUploadedUrl(fileName: string): Observable<string> {    
    return Observable.of(`https://lw1.blob.core.windows.net/test/${encodeURIComponent(fileName)}?st=2017-11-12T01%3A10%3A00Z&se=2019-12-13T01%3A10%3A00Z&sp=rwdl&sv=2017-04-17&sr=c&sig=nk%2FSeZhBYGA87X%2F0oyaYYyoFjxPbZ6jIpcbXoEjUuEE%3D`);                     
  }
}
