import { HttpClient, HttpEventType, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { Car } from '../models';
import { Subject } from 'rxjs';

@Injectable()
export class CarService {

  constructor(private httpClient: HttpClient) { }

  /**
   * @param fileName
   */
  private getFileUrl(fileName: string): string {    
    return `https://lw1.blob.core.windows.net/test/${encodeURIComponent(fileName)}?st=2017-11-12T01%3A10%3A00Z&se=2019-12-13T01%3A10%3A00Z&sp=rwdl&sv=2017-04-17&sr=c&sig=nk%2FSeZhBYGA87X%2F0oyaYYyoFjxPbZ6jIpcbXoEjUuEE%3D`;
  }

  public getCars(): Observable<Car[]> {
    return this.httpClient.get<Car[]>(this.getFileUrl('cars.json'), {
      responseType: 'json'
    });
  }

  public updateCars(cars: Car[]): Observable<boolean> {
    const req = new HttpRequest('PUT', this.getFileUrl('cars.json'), cars, {
      reportProgress: true,
      headers: new HttpHeaders({
        'x-ms-blob-type': 'BlockBlob'
      }),
      responseType: 'text'
    });

    const subject = new Subject<boolean>();
    this.httpClient.request(req).subscribe(evt => {
      
      if (evt.type === HttpEventType.UploadProgress) {
        console.log('Progress');
      } else if (evt.type === HttpEventType.Response) {
        subject.next(true);
      }
    }, err => {
      subject.error(err);
    });

    return subject.asObservable();
  }
}
