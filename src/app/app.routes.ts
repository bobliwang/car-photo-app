import { Routes } from '@angular/router';
import { CarDetailsComponent } from './car-details/car-details.component';
import { CarsListComponent } from './cars-list/cars-list.component';

export const appRoutes: Routes = [
    { path: '', component: CarsListComponent },
    { path: 'list', component: CarsListComponent },
    { path: 'details/:carId', component: CarDetailsComponent }
];